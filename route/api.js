const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const config = require('../config/database');

//create router
const router = express.Router();

const Todo = require('../model/todo');

//export router
module.exports = router;

router.post('/addTodo', (request, response)=>{
    console.log(request.body);
    let todo = Todo({title: request.body.title, description: request.body.description});
    Todo.addTodo(todo, (error,todo)=>{
        if(error) throw error;
        response.json({ success: true, msg : "Todo saved!!", todo: todo });
    });
});
