const express = require('express');
const path = require('path');
const PORT = 3000;
const app = express();
const mongoose = require('mongoose');
const config = require('./config/database');
const api = require('./route/api');
const bodyParser = require('body-parser');

//connecte to database
mongoose.connect(config.database, { useNewUrlParser: true });

//on connection
mongoose.connection.on('connected',()=>{
    console.log('MongoDb is connected to db : '+config.database);
});

//on error
mongoose.connection.on('error',(error)=>{
    console.log('Dtaabase connection errors : '+ error);
});

//use the api router
app.use('/api',api);

//use body-parser
app.use(bodyParser);

//create route
app.get('/', (req,res)=>{
    //res.send('Hello !!!!!!!!');
    res.sendfile(path.join(__dirname + '/public/index.html'));
});

//start server
app.listen(PORT, ()=>{
    console.log('Server started on port : '+PORT);
});