import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-add-todo',
  templateUrl: './add-todo.component.html',
  styleUrls: ['./add-todo.component.css']
})
export class AddTodoComponent implements OnInit {

  //initialize params for form
  title : string = "";
  description : string = "";
  listTodos : any [] = [];
  constructor(private http : HttpClient)  { }

  ngOnInit() {
    this.listTodo();
  }

  addTodo()
  {
    console.log('Title = '+this.title+'\n description = '+this.description);
    this.http.post("http://localhost:3000/api/addTodo",{
        title: this.title,
        description: this.description
    }).pipe(map(res=> res)).subscribe((data : any)=> {
        console.log(data);
    }); 
  }

  listTodo()
  {
    this.http.get("http://localhost:3000/api/listTodo").pipe(map(res=> res)).subscribe((data : any)=> {
        console.log(data);
        this.listTodos = data.todos;
    }); 
  }


}
