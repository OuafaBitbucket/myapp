const mongoose = require('mongoose');
const config = require('../config/database');
//create schema
const ToDoSchema = mongoose.Schema({
    title: {
        type: String,
        require: true
    },
    description: {
        type: String,
        require: true
    }
});

//export schema
const Todo = module.exports = mongoose.model('Todo', ToDoSchema);

//add todo object
module.exports.addTodo = function(todo, callback) {
    todo.save(callback);
}

//list todo object
module.exports.listTodo = function(callback) {
    Todo.find(callback);
}